// This is the js for the default/index.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    self.get_build = function () {


        $.getJSON(get_user_url, function(data) {
            self.vue.current_user = data.current_user;

            // if logged in get most recent build from user
            if (self.vue.current_user) {
                $.getJSON(get_build_url, 
                    {
                        user_email: self.vue.current_user.email
                    }, 
                    function (data) {

                    if (data.build) {
                        self.vue.build_name = data.build.name;

                        if (data.build.parts) {
                            self.vue.build_parts = JSON.parse(data.build.parts);
                        } else {
                            self.vue.build_parts = [];
                        }
                    }
                    
                })
            } else { // otherwise, get most recent build from local storage
                self.vue.build_parts = JSON.parse(localStorage.build_parts)
            }

        })

    };

    self.get_gpus = function () {
        $.getJSON(get_part_url(0, 4, "Graphics Card"), function (data) {
            self.vue.gpus = data.gpus;

            for (i = 0; i < data.gpus.length; i++) {
                self.vue.gpus[i].similar_items = JSON.parse(data.gpus[i].similar_items);
            }

            self.vue.has_more_gpu = data.has_more;
            self.vue.logged_in = data.logged_in;
            self.vue.current_user = data.current_user;

        })
    };

    self.get_cpus = function () {
        $.getJSON(get_part_url(0, 4, "Processor"), function (data) {
            self.vue.cpus = data.cpus;
            for (i = 0; i < data.cpus.length; i++) {
                self.vue.cpus[i].similar_items = JSON.parse(data.cpus[i].similar_items);
            }
            self.vue.has_more_cpu = data.has_more;
        })
    };

    self.get_rams = function () {
        $.getJSON(get_part_url(0, 4, "Memory"), function (data) {
            self.vue.rams = data.rams;
            for (i = 0; i < data.rams.length; i++) {
                self.vue.rams[i].similar_items = JSON.parse(data.rams[i].similar_items);
            }
            self.vue.has_more_memory = data.has_more;
        })
    };

    self.get_hdds = function () {
        $.getJSON(get_part_url(0, 4, "Storage"), function (data) {
            self.vue.hdds = data.hdds;
            for (i = 0; i < data.hdds.length; i++) {
                self.vue.hdds[i].similar_items = JSON.parse(data.hdds[i].similar_items);
            }
            self.vue.has_more_storage = data.has_more;
        })
    };

    self.get_mboards = function () {
        $.getJSON(get_part_url(0, 4, "Motherboard"), function (data) {
            self.vue.mboards = data.mboards;
            for (i = 0; i < data.mboards.length; i++) {
                self.vue.mboards[i].similar_items = JSON.parse(data.mboards[i].similar_items);
            }
            self.vue.has_more_motherboard = data.has_more;

        })
    };

    self.get_psus = function () {
        $.getJSON(get_part_url(0, 4, "Power Supply"), function (data) {
            self.vue.psus = data.psus;
            for (i = 0; i < data.psus.length; i++) {
                self.vue.psus[i].similar_items = JSON.parse(data.psus[i].similar_items);
            }
            self.vue.has_more_powersupply = data.has_more;
        })
    };

    function get_all_builds_url(start_idx, end_idx) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return all_builds_url + "?" + $.param(pp);
    }

    function get_part_url(start_idx, end_idx, part) {
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };

        switch(part) {
            case 'Processor':
                return cpus_url + "?" + $.param(pp);
                break;
            case 'Graphics Card':
                return gpus_url + "?" + $.param(pp);
                break;
            case 'Memory':
                return rams_url + "?" + $.param(pp);
                break;
            case 'Storage':
                return hdds_url + "?" + $.param(pp);
                break;
            case 'Motherboard':
                return mboards_url + "?" + $.param(pp);
                break;
            case 'Power Supply':
                return psus_url + "?" + $.param(pp);
                break;
            case 'All Builds':
                return all_builds_url + "?" + $.param(pp);
                break;
            default:
        }
        
    }


    self.get_more = function () {

        if (self.vue.page == "Graphics Card") {
            var num_gpus = self.vue.gpus.length;

            $.getJSON(get_part_url(num_gpus, num_gpus + 4, "Graphics Card"), function (data) {
                self.vue.has_more_gpu = data.has_more;                
                self.extend(self.vue.gpus, data.gpus);
                for (i = num_gpus, j=0; i < self.vue.gpus.length; i++, j++) {
                    self.vue.gpus[i].similar_items = JSON.parse(data.gpus[j].similar_items);
                }
            });
        }
        
        if (self.vue.page == "Processor") {
            var num_cpus = self.vue.cpus.length;

            $.getJSON(get_part_url(num_cpus, num_cpus + 4, "Processor"), function (data) {
                self.vue.has_more_cpu = data.has_more;
                self.extend(self.vue.cpus, data.cpus);
                for (i = num_cpus, j=0; i < self.vue.cpus.length; i++, j++) {
                    self.vue.cpus[i].similar_items = JSON.parse(data.cpus[j].similar_items);
                }
            });
        }        

        if (self.vue.page == "Memory") {
            var num_rams = self.vue.rams.length;

            $.getJSON(get_part_url(num_rams, num_rams + 4, "Memory"), function (data) {
                self.vue.has_more_memory = data.has_more;
                self.extend(self.vue.rams, data.rams);
                for (i = num_rams, j=0; i < self.vue.rams.length; i++, j++) {
                    self.vue.rams[i].similar_items = JSON.parse(data.rams[j].similar_items);
                }
            });
        }       

        if (self.vue.page == "Storage") {
            var num_hdds = self.vue.hdds.length;

            $.getJSON(get_part_url(num_hdds, num_hdds + 4, "Storage"), function (data) {
                self.vue.has_more_storage = data.has_more;
                self.extend(self.vue.hdds, data.hdds);
                for (i = num_hdds, j=0; i < self.vue.hdds.length; i++, j++) {
                    self.vue.hdds[i].similar_items = JSON.parse(data.hdds[j].similar_items);
                }
            });
        }        

        if (self.vue.page == "Motherboard") {
            var num_mboards = self.vue.mboards.length;

            $.getJSON(get_part_url(num_mboards, num_mboards + 4, "Motherboard"), function (data) {
                self.vue.has_more_motherboard = data.has_more;
                self.extend(self.vue.mboards, data.mboards);
                for (i = num_mboards, j=0; i < self.vue.mboards.length; i++, j++) {
                    self.vue.mboards[i].similar_items = JSON.parse(data.mboards[j].similar_items);
                }
            });
        }     

        if (self.vue.page == "Power Supply") {
            var num_psus = self.vue.psus.length;

            $.getJSON(get_part_url(num_psus, num_psus + 4, "Power Supply"), function (data) {
                self.vue.has_more_powersupply = data.has_more;
                self.extend(self.vue.psus, data.psus);
                for (i = num_psus, j=0; i < self.vue.psus.length; i++, j++) {
                    self.vue.psus[i].similar_items = JSON.parse(data.psus[j].similar_items);
                }
            });
        }     

        if (self.vue.page == "All Builds") {

            var num_builds = self.vue.all_builds.length;

            $.getJSON(get_part_url(num_builds, num_builds + 4, "All Builds"), function(data) {
                self.vue.has_more_builds = data.has_more;
                self.extend(self.vue.all_builds, data.all_builds);
                for (i = num_builds, j=0; i < self.vue.all_builds.length; i++, j++) {
                    self.vue.all_builds[i].parts = JSON.parse(data.all_builds[j].parts)
                }
            });


        }
        
    };

    self.toggle_expand = function (i, type, event) {

        if (event.target.className != 'btn btn-add') {
            switch(type) {
                case "Graphics Card":
                    self.vue.gpus[i].expanded = !self.vue.gpus[i].expanded;
                    self.render_benchmark_comparison(i, type);
                    break;
                case "Processor":
                    self.vue.cpus[i].expanded = !self.vue.cpus[i].expanded;
                    self.render_benchmark_comparison(i, type);
                    break;
                case "Memory":
                    self.vue.rams[i].expanded = !self.vue.rams[i].expanded;
                    self.render_benchmark_comparison(i, type);
                    break;
                case "Storage":
                    self.vue.hdds[i].expanded = !self.vue.hdds[i].expanded;
                    self.render_benchmark_comparison(i, type);
                    break;
                case "Motherboard":
                    self.vue.mboards[i].expanded = !self.vue.mboards[i].expanded;
                    self.render_benchmark_comparison(i, type);
                    break;
                case "Power Supply":
                    self.vue.psus[i].expanded = !self.vue.psus[i].expanded;
                    self.render_benchmark_comparison(i, type);
                    break;
                default:
            }
        }
        
    }

    self.render_benchmark_comparison = function (i, type) {

        switch(type) {
            case "Graphics Card":
                var max = 12000; 

                self.vue.gpus[i].width = Math.ceil(self.vue.gpus[i].benchmark) * 100 / max 

                for (j = 0; j < self.vue.gpus[i].similar_items.length; j++) {
                    var width = Math.ceil(self.vue.gpus[i].similar_items[j].benchmark) * 100 / max;
                    self.vue.gpus[i].similar_items[j].width = width;
                }
                break;
            case "Processor":
                var max = 20000; 

                self.vue.cpus[i].width = Math.ceil(self.vue.cpus[i].benchmark) * 100 / max 

                for (j = 0; j < self.vue.cpus[i].similar_items.length; j++) {
                    var width = Math.ceil(self.vue.cpus[i].similar_items[j].benchmark) * 100 / max;
                    self.vue.cpus[i].similar_items[j].width = width;
                }
                break;
            case "Memory":
                var max = 50; 

                self.vue.rams[i].width = Math.ceil(self.vue.rams[i].benchmark) * 100 / max 

                for (j = 0; j < self.vue.rams[i].similar_items.length; j++) {
                    var width = Math.ceil(self.vue.rams[i].similar_items[j].benchmark) * 100 / max;
                    self.vue.rams[i].similar_items[j].width = width;
                }
                break;
            case "Storage":
                var max = 30; 

                self.vue.hdds[i].width = Math.ceil(self.vue.hdds[i].benchmark) * 100 / max 

                for (j = 0; j < self.vue.hdds[i].similar_items.length; j++) {
                    var width = Math.ceil(self.vue.hdds[i].similar_items[j].benchmark) * 100 / max;
                    self.vue.hdds[i].similar_items[j].width = width;
                }
                break;
            case "Motherboard":
                var max = 120; 

                self.vue.mboards[i].width = Math.ceil(self.vue.mboards[i].benchmark) * 100 / max 

                for (j = 0; j < self.vue.mboards[i].similar_items.length; j++) {
                    var width = Math.ceil(self.vue.mboards[i].similar_items[j].benchmark) * 100 / max;
                    self.vue.mboards[i].similar_items[j].width = width;
                }
                break;
            case "Power Supply":
                var max = 120; 

                self.vue.psus[i].width = Math.ceil(self.vue.psus[i].benchmark) * 100 / max 

                for (j = 0; j < self.vue.psus[i].similar_items.length; j++) {
                    var width = Math.ceil(self.vue.psus[i].similar_items[j].benchmark) * 100 / max;
                    self.vue.psus[i].similar_items[j].width = width;
                }
                break;
            default:

        }

    }

    self.insert_build = function () {

        if (self.vue.build_parts.length > 0) {

            $.post(insert_build_url,
            {
                prev_name: self.vue.build_name,
                parts: [],
                user_email: self.vue.current_user.email
            },
            function (data) {
                self.vue.build_parts = [],
                self.vue.build_name = "My New Build"
                self.vue.my_builds = data.my_builds
                for (i = 0; i < data.my_builds.length; i++) {
                    self.vue.my_builds[i].parts = JSON.parse(data.my_builds[i].parts);
                }
            });
        }
        
    }

    self.update_build_name = function() {

        $.post(update_build_name_url,
            {
                name: self.vue.build_name,
                user_email: self.vue.current_user.email
            },
            function (data) {
            });
    }

    self.add_to_build = function (i) {

        if (self.vue.page == 'Graphics Card') {
            var part = {
                name: self.vue.gpus[i].name,
                image: self.vue.gpus[i].image,
                core_clock: self.vue.gpus[i].core_clock,
                part_type: self.vue.gpus[i].part_type,
                price: self.vue.gpus[i].price
            }
        }

        if (self.vue.page == 'Processor') {
            var part = {
                name: self.vue.cpus[i].name,
                image: self.vue.cpus[i].image,
                op_freq: self.vue.cpus[i].op_freq,
                part_type: self.vue.cpus[i].part_type,
                price: self.vue.cpus[i].price
            }
        }

        if (self.vue.page == 'Memory') {
            var part = {
                name: self.vue.rams[i].name,
                image: self.vue.rams[i].image,
                part_type: self.vue.rams[i].part_type,
                price: self.vue.rams[i].price
            }
        }

        if (self.vue.page == 'Storage') {
            var part = {
                name: self.vue.hdds[i].name,
                image: self.vue.hdds[i].image,
                part_type: self.vue.hdds[i].part_type,
                price: self.vue.hdds[i].price
            }
        }

        if (self.vue.page == 'Motherboard') {
            var part = {
                name: self.vue.mboards[i].name,
                image: self.vue.mboards[i].image,
                part_type: self.vue.mboards[i].part_type,
                price: self.vue.mboards[i].price
            }
        }

        if (self.vue.page == 'Power Supply') {
            var part = {
                name: self.vue.psus[i].name,
                image: self.vue.psus[i].image,
                part_type: self.vue.psus[i].part_type,
                price: self.vue.psus[i].price
            }
        }
        

        self.vue.build_parts.unshift(part)

        if(!self.vue.current_user) {
            localStorage.build_parts = JSON.stringify(self.vue.build_parts)
        }

        if (self.vue.current_user) {
            $.post(add_to_build_url,
            {
                name: self.vue.build_name,
                user_email: self.vue.current_user.email,
                parts: JSON.stringify(self.vue.build_parts)
            },
            function (data) {
                self.vue.build_parts = JSON.parse(data.build.parts)
            });
        }

        
    };

    self.delete_from_build = function (i) {

        self.vue.build_parts.splice(i, 1);

        if (self.vue.current_user) {

            $.post(del_from_build_url,
            {
                parts: JSON.stringify(self.vue.build_parts)
            },
            function (data) {
                self.vue.build_parts = JSON.parse(data.build.parts)
            });
        } else {

            localStorage.build_parts = JSON.stringify(self.vue.build_parts)
        }
        
    };

    self.del_build = function (i) {
        $.post(del_build_url,
            {
                build_id: self.vue.my_builds[i].id
            },
            function (data) {
                self.vue.my_builds.splice(i, 1);
            });
    }

    self.clear_build_parts = function() {
        self.vue.build_parts = JSON.parse(localStorage.build_parts)
    }

    self.goto = function (page) {
        self.vue.page = page;

        if (page == "My Builds") {

            $.getJSON(get_my_builds_url,
            { 
                user_email: self.vue.current_user.email
            },
            function (data) {
                if (data.my_builds) {
                    self.vue.my_builds = data.my_builds;
                    for (i = 0; i < data.my_builds.length; i++) {
                        self.vue.my_builds[i].parts = JSON.parse(data.my_builds[i].parts);
                    }
                } else {
                    self.vue.my_builds = [];
                }
                
            });
        }

        if (page == "All Builds") {

            $.getJSON(get_part_url(0, 8, "All Builds"), function(data) {
                self.vue.has_more_builds = data.has_more;
                self.vue.all_builds = data.all_builds;
                for (i = 0; i < self.vue.all_builds.length; i++) {
                    self.vue.all_builds[i].parts = JSON.parse(data.all_builds[i].parts)
                }
            });
        }
    }

    self.view_my_build = function(build) {
        self.vue.page = 'Viewing ' + "'" + build.name +"'";
        self.vue.build_view = "My Build";
        self.vue.viewing_build = build;
    }

    self.view_all_build = function(build) {
        self.vue.page = 'Viewing ' + "'" + build.name +"'";
        self.vue.build_view = "All Build";
        self.vue.viewing_build = build;
    }

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            build_view: "",
            has_more_gpu: true,
            has_more_cpu: true,
            has_more_memory: true,
            has_more_storage: true,
            has_more_motherboard: true,
            has_more_powersupply: true,
            has_more_builds: true,
            logged_in: false,
            name: null,
            all_builds: [],
            gpus: [],
            cpus: [],
            rams: [],
            hdds: [],
            mboards: [],
            psus: [],
            viewing_build: {},
            similar_items: [],
            my_builds: [],
            build_name: "My Build",
            build_parts: [],
            current_user: "",
            page: "Processor"
        },
        methods: {
            get_more: self.get_more,
            add_to_build: self.add_to_build,
            delete_from_build: self.delete_from_build,
            goto: self.goto,
            toggle_expand: self.toggle_expand,
            render_benchmark_comparison: self.render_benchmark_comparison,
            insert_build: self.insert_build,
            view_my_build: self.view_my_build,
            view_all_build: self.view_all_build,
            del_build: self.del_build,
            update_build_name: self.update_build_name,
            clear_build_parts: self.clear_build_parts
        }

    });

    self.get_gpus();
    self.get_cpus();
    self.get_rams();
    self.get_mboards();
    self.get_hdds();
    self.get_psus();
    self.get_build();

    $("#vue-div").show()

    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
window.onload = jQuery(function(){APP = app();});
