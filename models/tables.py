# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.

import datetime

db.define_table('post',
                Field('user_email', default=auth.user.email if auth.user_id else None),
                Field('post_content', 'text'),
                Field('created_on', 'datetime', default=datetime.datetime.utcnow()),
                Field('updated_on', 'datetime', update=datetime.datetime.utcnow()),
                )

# I don't want to display the user email by default in all forms.
db.post.user_email.readable = db.post.user_email.writable = False
db.post.post_content.requires = IS_NOT_EMPTY()
db.post.created_on.readable = db.post.created_on.writable = False
db.post.updated_on.readable = db.post.updated_on.writable = False

# after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

# Example for data formatting
#
# db.gpu.insert(
#       name='GeForce 1080 GTX',
#       partnum='F4AFE322',
#       image='http://cdn.pcpartpicker.com/23322.jpg',
#       price=545.99
#       core_clock=1.65
#       memory_type='GDDR3',
#       memory_size=6
#       interface='PCI Express x16',
#       tdp=35
#       dual_dvi=3
#       hdmi=1
#       displayport=1,
#       benchmark=76,
#       similar_items='[{"name": "GeForce 1070 GT", "benchmark": 65}, {"name": "GeForce 1070 GT", #       "benchmark": 65}, {"name": "GeForce 1070 GT", "benchmark": 65}]'
#)


db.define_table('gpu',
                Field('part_type', 'string', default='gpu'), # don't include
                Field('name', 'string'),
                Field('part_num', 'string'),
                Field('image', 'text'),
                Field('price', 'double'),
                Field('core_clock', 'double'),
                Field('memory_type', 'string'),
                Field('memory_size', 'integer'),
                Field('interface', 'string'),
                Field('tdp', 'integer'), #watts
                Field('dual_dvi', 'integer'),
                Field('displayport', 'integer'),
                Field('hdmi', 'integer'),
                Field('similar_items', 'json', default={}), #ie [{"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}]
                Field('benchmark', 'double'), # 3D Marks Graphics Score
                Field('created_on', 'datetime', default=datetime.datetime.utcnow())
                )

db.define_table('cpu',
                Field('part_type', 'string', default='cpu'),
                Field('name', 'string'),
                Field('part_num', 'string'),
                Field('image', 'text'),
                Field('price', 'double'),
                Field('op_freq', 'double'),
                Field('turbo_freq', 'double'),
                Field('socket', 'string'),
                Field('cores', 'integer'),
                Field('l1_cache', 'string'),
                Field('l2_cache', 'string'),
                Field('l3_cache', 'string'),
                Field('cpu_power', 'integer'), #watts
                Field('similar_items', 'json', default={}), #ie [{"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}]
                Field('benchmark', 'double'), #Passmark benchmark score
                Field('created_on', 'datetime', default=datetime.datetime.utcnow())
                )

db.define_table('ram',
                Field('part_type', 'string', default='ram'),
                Field('name', 'string'),
                Field('part_num', 'string'),
                Field('image', 'text'),
                Field('price', 'double'),
                Field('ram_type', 'string'), #ie. 288-pin DIMM
                Field('speed', 'string'),
                Field('ram_size', 'integer'), #ie 16 GB
                Field('cas_latency', 'integer'),
                Field('timing', 'string'),
                Field('voltage', 'double'), #volts
                Field('similar_items', 'json', default={}), #ie [{"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}]
                Field('benchmark', 'double'), #price/GB
                Field('created_on', 'datetime', default=datetime.datetime.utcnow())
                )

db.define_table('motherboard',
                Field('part_type', 'string', default='mboard'),
                Field('name', 'string'),
                Field('part_num', 'string'),
                Field('image', 'text'),
                Field('price', 'double'),
                Field('form_factor', 'string'), #ie. ATX
                Field('cpu_socket', 'string'),
                Field('chipset', 'string'), 
                Field('memory_type', 'string'),
                Field('max_ram', 'string'),
                Field('raid_support', 'boolean'), 
                Field('crossfire_support', 'boolean'),
                Field('similar_items', 'json', default={}), #ie [{"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}]
                Field('benchmark', 'double'), 
                Field('created_on', 'datetime', default=datetime.datetime.utcnow())
                )

db.define_table('hdd',
                Field('part_type', 'string', default='hdd'),
                Field('name', 'string'),
                Field('part_num', 'string'),
                Field('image', 'text'),
                Field('price', 'double'),
                Field('capacity', 'string'), #include space before unit ie. "1 TB", "500 GB"
                Field('interface', 'string'), 
                Field('cache_size', 'string'), #MB
                Field('rpm', 'string'),
                Field('form_factor', 'string'),
                Field('similar_items', 'json', default={}), #ie [{"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}]
                Field('benchmark', 'string'), #read/write speeds in MB/s ie. "195/193" 
                Field('created_on', 'datetime', default=datetime.datetime.utcnow())
                )

db.define_table('psu',
                Field('part_type', 'string', default='psu'),
                Field('name', 'string'),
                Field('part_num', 'string'),
                Field('image', 'text'),
                Field('price', 'double'),
                Field('psu_type', 'string'), 
                Field('modular', 'string'), 
                Field('wattage', 'double'), 
                Field('fans', 'integer'),
                Field('efficiency', 'string'), #ie. 80++ Gold
                Field('similar_items', 'json', default={}), #ie [{"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}, {"name": "EVGA GeForce 1080 GTX", "benchmark": "3640"}]
                Field('benchmark', 'double'), #efficiency + wattage
                Field('created_on', 'datetime', default=datetime.datetime.utcnow())
                )


db.define_table('build',
                Field('user_email', default=auth.user.email if auth.user_id else 'anonymous'),
                Field('parts', 'json', default=[]),
                Field('name', 'text', default='My build'),
                Field('created_on', 'datetime', default=datetime.datetime.utcnow()),
                Field('updated_on', 'datetime', update=datetime.datetime.utcnow()),
                Field('temp_build')
                )

# db.define_table('gpu',
#                 Field('image', 'text'),
#                 Field('manufacturer', 'text'),
#                 Field('part_num', 'text'),
#                 Field('interface', 'text'),
#                 Field('chipset', 'text'),
#                 Field('memory', 'integer'),
#                 Field('memory_type', 'text'),
#                 Field('core_clock', 'double'),
#                 Field('tdp', 'integer'),
#                 Field('fan', 'text'),
#                 Field('length', 'text'),
#                 Field('dvi', 'integer'),
#                 Field('displayport', 'integer'),
#                 Field('hdmi', 'integer')
#                 )