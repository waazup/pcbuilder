# These are the controllers for your ajax api.

import datetime
import json
import unicodedata

def index():
    pass

def get_user_name_from_email(email):
    """Returns a string corresponding to the user first and last names,
    given the user email."""
    u = db(db.auth_user.email == email).select().first()
    if u is None:
        return 'None'
    else:
        return ' '.join([u.first_name, u.last_name])

def get_posts():
    """This controller is used to get the posts.  Follow what we did in lecture 10, to ensure
    that the first time, we get 4 posts max, and each time the "load more" button is pressed,
    we load at most 4 more posts."""
    # Implement me!
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    posts = []
    has_more = False
    rows = db().select(db.post.ALL, orderby=~db.post.created_on, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            p = dict(
                id = r.id,
                post_content = r.post_content,
                edit_content = r.post_content,
                created_on = r.created_on,
                user_email = r.user_email,
                author = get_user_name_from_email(r.user_email),
                updated_on = r.updated_on,
                is_editing = False
            )
            posts.append(p)
        else:
            has_more = True
    logged_in = auth.user_id is not None
    return response.json(dict(
        current_user=auth.user,
        posts=posts,
        logged_in=logged_in,
        has_more=has_more,
    ))


# Note that we need the URL to be signed, as this changes the db.
@auth.requires_signature()
def add_post():
    """Here you get a new post and add it.  Return what you want."""
    # Implement me!
    p_id = db.post.insert(
        post_content=request.vars.post_content
    )
    p = db.post(p_id)
    p.is_editing = 0
    p.edit_content = p.post_content
    p.author = get_user_name_from_email(p.user_email)
    return response.json(dict(post=p))

@auth.requires_signature()
def edit_post():
    row = db(db.post.id == request.vars.post_id).select().first()
    row.update_record(post_content=request.vars.edit_content)
    row.update_record(updated_on=datetime.datetime.utcnow())
    return response.json(dict(post=row))

@auth.requires_signature()
def del_post():
    """Used to delete a post."""
    # Implement me!
    db(db.post.id == request.vars.post_id).delete()

@auth.requires_signature()
def add_gpu():
    """Here you get a new post and add it.  Return what you want."""
    # Implement me!
    g_id = db.gpu.insert(
        name=request.vars.name,
        image=request.vars.image,
        core_clock=request.vars.core_clock,
        part_type='gpu'
        )
    g = db.post(g_id)
    return response.json(dict(gpu=g))

def add_to_build():
    row = db(db.build.user_email == request.vars.user_email).select(db.build.ALL).last()
    if (row):
        row.update_record(parts=request.vars.parts, name=request.vars.name)
    else:
        b_id = db.build.insert(
                parts = request.vars.parts, 
                user_email = request.vars.user_email,
                name = request.vars.name
            )
        row = db.post(b_id)
    return response.json(dict(build=row))

def update_build_name():
    row = db(db.build.user_email == request.vars.user_email).select(db.build.ALL).last()
    if (row):
        row.update_record(name=request.vars.name)
    # return response.json(dict(build=row))

def insert_build():
    row = db(db.build.user_email == request.vars.user_email).select(db.build.ALL).last()
    if (row):
        logger.info(row)
        row.update_record(name=request.vars.prev_name)
        row.update_record(temp_build=False)
        row.update_record(created_on=datetime.datetime.utcnow())

    my_builds = db((db.build.user_email == request.vars.user_email) & (db.build.temp_build == False)).select(db.build.ALL, orderby=~db.build.created_on)

    b_id = db.build.insert(
            parts = request.vars.parts, 
            user_email = request.vars.user_email,
            name = "My Build",
            temp_build = True
        )

    build = db.post(b_id)

    return response.json(dict(my_builds=my_builds))
    

def get_my_builds():
    my_builds = []

    logger.info(request.vars.user_email)

    most_recent = db(db.build.user_email == request.vars.user_email).select(db.build.ALL).last()

    # rows = db((db.build.user_email == request.vars.user_email) and (db.build.parts != []) and (db.build.id != most_recent.id)).select(db.build.ALL, orderby=~db.build.created_on).as_list()

    rows = db((db.build.user_email == request.vars.user_email) & (db.build.parts != []) & (db.build.temp_build == False)).select(db.build.ALL, orderby=~db.build.created_on).as_list()

    logger.info(rows)

    return response.json(dict(
        my_builds=rows
    ))

def get_build():

    b = db(db.build.user_email == request.vars.user_email).select(db.build.ALL).last()   
    logger.info(b)
    if b is not None: 
        build = dict(
                    parts = b.parts, 
                    user_email = b.user_email,
                    name = b.name, 
                    created_on = b.created_on,
                    updated_on = b.updated_on
                )
        return response.json(dict(
            build=build
        ))

def del_build():
    db(db.build.id == request.vars.build_id).delete()

def del_from_build():
    row = db().select(db.build.ALL).last()
    row.update_record(parts=request.vars.parts)
    return response.json(dict(build=row))

def init_build():
    db.build.insert(name='My build', parts=[])

def get_similar_items():
    similar_items = []
    if request.vars.type == 'Graphics Card':
        gpus = db().select(db.gpu.ALL)
        for i, g in enumerate(gpus):
            if g.price <= request.vars.price and len(similar_items) < 3:
                gpu = dict(
                    name = g.name,
                    benchmark = g.benchmark
                )
                similar_items.append(gpu)
    return response.json(dict(
        similar_items=similar_items
    ))

def get_user():
    return response.json(dict(current_user=auth.user))

def get_all_builds():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0

    all_builds = []
    has_more = False
    rows = db((db.build.parts != []) & (db.build.temp_build == False)).select(db.build.ALL, limitby=(start_idx, end_idx + 1), orderby=~db.build.created_on)
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            b = dict(
                id = r.id,
                name = r.name,
                parts = r.parts,
                created_on = r.created_on, 
                user_email = r.user_email
            ) 
            all_builds.append(b)
        else: 
            has_more = True

    return response.json(dict(
        all_builds=all_builds,
        has_more=has_more
    ))


def get_gpus():
    """This controller is used to get the posts.  Follow what we did in lecture 10, to ensure
    that the first time, we get 4 posts max, and each time the "load more" button is pressed,
    we load at most 4 more posts."""
    # Implement me!
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    gpus = []
    has_more = False
    rows = db().select(db.gpu.ALL, limitby=(start_idx, end_idx + 1))

    for i, r in enumerate(rows):
        if i < end_idx - start_idx:


            images = []

            similar = json.loads(r.similar_items)
            

            for j, item in enumerate(similar):
                # unicodedata.normalize('NFKD', title).encode('ascii','ignore')

                card = db(db.gpu.name == item[u'name']).select(db.gpu.ALL)
                logger.info(item[u'name'])
                
                for c in card:
                    images.append(c.image)



            


            g = dict(
                id = r.id,
                name = r.name,
                image = r.image,
                price = r.price,
                part_num = r.part_num,
                memory_size = r.memory_size,
                memory_type = r.memory_type,
                interface = r.interface,
                tdp = r.tdp,
                dual_dvi = r.dual_dvi,
                displayport = r.displayport,
                hdmi = r.hdmi,
                core_clock = r.core_clock,
                part_type = r.part_type,
                created_on = r.created_on,
                similar_items = r.similar_items,
                benchmark = r.benchmark,
                expanded = False,
                images = images
            )
            gpus.append(g)
        else:
            has_more = True
    logged_in = auth.user_id is not None
    return response.json(dict(
        start_idx=start_idx,
        current_user=auth.user,
        gpus=gpus,
        logged_in=logged_in,
        has_more=has_more,
    ))

def get_cpus():
    """This controller is used to get the posts.  Follow what we did in lecture 10, to ensure
    that the first time, we get 4 posts max, and each time the "load more" button is pressed,
    we load at most 4 more posts."""
    # Implement me!
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    cpus = []
    has_more = False
    rows = db().select(db.cpu.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:

            # images = []

            # similar = json.loads(r.similar_items)
            

            # for j, item in enumerate(similar):
            #     # unicodedata.normalize('NFKD', title).encode('ascii','ignore')

            #     card = db(db.cpu.name == item[u'name']).select(db.cpu.ALL)
            #     logger.info(item[u'name'])
                
            #     for c in card:
            #         images.append(c.image)


            c = dict(
                id = r.id,
                name = r.name,
                image = r.image,
                price = r.price,
                cores = r.cores,
                op_freq = r.op_freq,
                turbo_freq = r.turbo_freq,
                socket = r.socket,
                l1_cache = r.l1_cache,
                l2_cache = r.l2_cache,
                l3_cache = r.l3_cache,
                cpu_power = r.cpu_power,
                part_num = r.part_num,
                part_type = r.part_type,
                created_on = r.created_on,
                similar_items = r.similar_items,
                benchmark = r.benchmark,
                expanded = False
            )
            cpus.append(c)
        else:
            has_more = True
    return response.json(dict(
        cpus=cpus,
        has_more=has_more
    ))

def get_rams():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    rams = []
    has_more = False
    rows = db().select(db.ram.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            m = dict(
                id = r.id,
                name = r.name,
                image = r.image,
                price = r.price,
                ram_type = r.ram_type,
                ram_size = r.ram_size,
                part_num = r.part_num,
                speed = r.speed,
                cas_latency = r.cas_latency,
                timing = r.timing,
                voltage = r.voltage,
                similar_items = r.similar_items,
                benchmark = r.benchmark,
                expanded = False
            )
            rams.append(m)
        else:
            has_more = True
    return response.json(dict(
        rams=rams,
        has_more=has_more
    ))

def get_hdds():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    hdds = []
    has_more = False
    rows = db().select(db.hdd.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            b = dict(
                id = r.id,
                name = r.name,
                image = r.image,
                price = r.price,
                part_num = r.part_num,
                form_factor = r.form_factor,
                interface = r.interface,
                cache_size = r.cache_size,
                rpm = r.rpm,
                capacity = r.capacity,
                similar_items = r.similar_items,
                benchmark = r.benchmark,
                expanded = False
            )
            hdds.append(b)
        else:
            has_more = True
    return response.json(dict(
        hdds=hdds,
        has_more=has_more
    ))

def get_mboards():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    mboards = []
    has_more = False
    rows = db().select(db.motherboard.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            b = dict(
                id = r.id,
                name = r.name,
                image = r.image,
                price = r.price,
                part_num = r.part_num,
                form_factor = r.form_factor,
                cpu_socket = r.cpu_socket,
                chipset = r.chipset,
                memory_type = r.memory_type,
                max_ram = r.max_ram,
                raid_support = r.raid_support,
                crossfire_support = r.crossfire_support,
                similar_items = r.similar_items,
                benchmark = r.benchmark,
                expanded = False
            )
            mboards.append(b)
        else:
            has_more = True
    return response.json(dict(
        mboards=mboards,
        has_more=has_more
    ))

def get_psus():
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    # We just generate a lot of of data.
    psus = []
    has_more = False
    rows = db().select(db.psu.ALL, limitby=(start_idx, end_idx + 1))
    for i, r in enumerate(rows):
        if i < end_idx - start_idx:
            b = dict(
                id = r.id,
                name = r.name,
                image = r.image,
                price = r.price,
                part_num = r.part_num,
                psu_type = r.psu_type,
                modular = r.modular,
                wattage = r.wattage,
                fans = r.fans,
                efficiency = r.efficiency,
                similar_items = r.similar_items,
                benchmark = r.benchmark,
                expanded = False
            )
            psus.append(b)
        else:
            has_more = True
    return response.json(dict(
        psus=psus,
        has_more=has_more
    ))

    
